﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var sql = "select * from Student";

            var dt = DBhelper.GetDataTable(sql);

            dataGridView1.DataSource = dt;//赋给数据源
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;//选择一整行
            dataGridView1.ReadOnly = true; // 表格不会被编辑
            dataGridView1.AllowUserToAddRows = false; // 不会显示新的一行
        }

        //查找
        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string .Format("select * from Student where studentname like '%{0}%'", name);
            var dt = DBhelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }

        //添加
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
           var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select * from Student";

                var dt = DBhelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("no");
            }


        }

        //更新
        private void button3_Click(object sender, EventArgs e)
        {
            var Id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["studentname"].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells["studentscore"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["studentage"].Value;
            Form2 form2 = new Form2(Id,name,score,age);
            var res = form2.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var sql = "select * from Student";

                var dt = DBhelper.GetDataTable(sql);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("no");
            }

        }

        //删除
        private void button4_Click(object sender, EventArgs e)
        {
            var Id = (int)dataGridView1.SelectedRows[0].Cells["id"].Value;
            var sql = string.Format("delete * from Student where id ={0}",Id);
                
        }
    }
}
